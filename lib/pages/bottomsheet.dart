import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'data.dart';

class BottomSheetExample extends StatefulWidget {
  const BottomSheetExample({super.key});

  @override
  State<BottomSheetExample> createState() => _BottomSheetExampleState();
}

class _BottomSheetExampleState extends State<BottomSheetExample> {
  String dropdownvalue = 'GGP';
  var items = [
    'GGP',
    'AM',
    'E',
    'NAWI',
    'NVS',
    'POS',
  ];

  DateTime selectedDate = DateTime.now();

  int time = 0;

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Column(

        children: <Widget>[
          DropdownButton<String>(
            value: dropdownvalue,
            style: const TextStyle(color: Colors.deepPurple),


            items: items.map((String items) {
              return DropdownMenuItem<String>(
                value: items,
                child: Text(items),
              );
            }).toList(),
            onChanged: (String? newValue) {
              setState(() {
                dropdownvalue = newValue!;
              });
            },
          ),
          TextField(
            keyboardType: TextInputType.number,
            onChanged: (value) {
              time = int.parse(value);
            },
          ),
          TextButton(
            onPressed: () async {
              final DateTime? picked = await showDatePicker(
                context: context,
                initialDate: DateTime.now(),
                firstDate: DateTime(2020, 1),
                lastDate: DateTime(2201),
              );
              if (picked != null && picked != selectedDate)
                setState(() {
                  selectedDate = picked;
                });
            },

            child: Text('${selectedDate.toLocal()}'.split(' ')[0]),

          ),
          Padding(
            padding: const EdgeInsets.all(15.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                FloatingActionButton(onPressed: () {

                  Data data = new Data(
                    time: time,
                    date: selectedDate,
                    subject: dropdownvalue,
                  );

                  Navigator.pop(context, data);
                },
                  child:  const Icon(Icons.add),
                ),
              ],
            ),
          )
        ],
      ),

    );
  }
}
