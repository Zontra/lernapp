import 'dart:collection';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'bottomsheet.dart';
import 'data.dart';

class Home extends StatefulWidget {
  const Home({Key? key}) : super(key: key);

  @override
  State<Home> createState() => _homeState();
}

class _homeState extends State<Home> {
  List<Data> list = [];
  Map<String, int> allsubjects = new HashMap();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        actions: [
          IconButton(
            icon: const Icon(Icons.add),
            iconSize: 40,
            onPressed: () {
              showModalBottomSheet<void>(
                context: context,
                builder: (BuildContext context) {
                  return const BottomSheetExample();
                },
              ).then((value) => {
                setState(() {
                  list.add(value as Data);
                })
              }
              );
            },
          )
        ],
        title: Text('My studies'),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                ...list.map((e) {
                  if (allsubjects.containsKey(e.subject)) {
                    allsubjects.update(e.subject, (value) => value + e.time);
                  } else {
                    allsubjects.addAll({e.subject: e.time});
                  }
                  return Container(
                      width: 40,
                      height: 200,
                      child: Center(
                          child: Column(
                                children: [
                                  Text(e.subject),
                                  Text(e.time.toString()),
                                  Container(
                                    height: 150,
                                    width: 10,
                                    child: RotatedBox(
                                      quarterTurns: 3,
                                      child:
                                      LinearProgressIndicator(
                                        value: allsubjects[e.subject]! / sumtime(list),
                                        backgroundColor: Colors.grey,
                                        valueColor: AlwaysStoppedAnimation<Color>(Colors.blue),
                                      ),
                                    ),
                                  )
                                ],
                              ),
                          )
                      );
                }).toList(),
              ],
            ),



            ...list.map((e) {
              return Column(
                children: [
                Card(
              elevation: 0,
              color: Theme.of(context).colorScheme.surfaceVariant,
              child: ListTile(
                leading: Container(
                  width: 50,
                  height: 50,
                  decoration: BoxDecoration(
                    color: Colors.blue,
                    borderRadius: BorderRadius.circular(10),

                  ),
                  child: Center(
                    child: Text(e.time.toString(),
                  ),
                ),
                ),
                title: Text(e.subject),
                subtitle: Text('${e.date.toLocal()}'.split(' ')[0]),
                trailing: IconButton(onPressed: () {
                  setState(() {
                    list.remove(e);
                  });
                }, icon: const Icon(Icons.delete)),
              ),
            ),
              ]);
            }).toList()
          ],

        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          showModalBottomSheet<void>(
            context: context,
            builder: (BuildContext context) {
              return const BottomSheetExample();
            },
          ).then((value) => {
              setState(() {
                list.add(value as Data);
              })
            }
          );
        },
        tooltip: 'Increment',
        child: const Icon(Icons.add),

      ),
    );
  }

  int sumtime(List<Data> list) {
    int sum = 0;
    for (int i = 0; i < list.length; i++) {
      sum += list[i].time;
    }
    return sum;
  }
}

