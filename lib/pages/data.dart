class Data {
  String subject = "";
  DateTime date;
  int time = 0;

  Data({required this.subject, required this.date, required this.time});
}